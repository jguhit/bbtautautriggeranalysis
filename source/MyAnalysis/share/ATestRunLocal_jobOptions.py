#See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake for more details about anything here
#Datapath='/afs/cern.ch/work/j/jguhit/quicktriggeranalysis/samples'
#testFile = os.path.join(Datapath, 'mc16_13TeV.600023.PhH7EG_PDF4LHC15_HHbbtautauHadHad_cHHH01d0.recon.AOD.e8269_e7400_s3126_r12406/AOD.25557808._001099.pool.root.1')

#path='/afs/cern.ch/work/j/jguhit/athanalysisr22/bbtautautriggeranalysis/samples/mc16_13TeV.600023.PhH7EG_PDF4LHC15_HHbbtautauHadHad_cHHH01d0.recon.AOD.e8269_e7400_s3126_r12406'
#testFile = [
#    os.path.join(path,'AOD.25557808._000124.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._001240.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._000198.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._000697.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._000308.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._001167.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._001432.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._000741.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._001434.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._001270.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._001383.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._001309.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._001266.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._000770.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._000770.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._001066.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._000577.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._000188.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._000695.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._001622.pool.root.1'), 
#    os.path.join(path,'AOD.25557808._000991.pool.root.1')
#    ]

#testFile = os.getenv("ALRB_TutorialData") + '/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6337_s3126_r10201_p4172/DAOD_PHYS.21569875._001323.pool.root.1'
#import AthenaRootComps.ReadAthenaxAOD
###R22 Samples
Datapath='/afs/cern.ch/work/j/jguhit/quicktriggeranalysis/sample_r13201'
testFile = os.path.join(Datapath,'mc20_13TeV.600023.PhH7EG_PDF4LHC15_HHbbtautauHadHad_cHHH01d0.recon.AOD.e7954_e7400_s3126_d1729_r13201/AOD.27382945._000067.pool.root.1')

#Datapath='/afs/cern.ch/work/j/jguhit/athanalysisr22/bbtautautriggeranalysis/samples'
#testFile = os.path.join(Datapath, 'valid1.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.recon.AOD.e5112_s3214_d1738_r13391_tid28072335_00/AOD.28072335._000318.pool.root.1')
#testFile = os.path.join(Datapath, 'data18_13TeV.00360026.physics_EnhancedBias.merge.AOD.r13464_p5024_tid28328499_00/AOD.28328499._000115.pool.root.1')

#override next line on command line with: --filesInput=XXX
#jps.AthenaCommonFlags.FilesInput = testFile #
jps.AthenaCommonFlags.FilesInput = [testFile]
#Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "ClassAccess" 

jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:MyxAODAnalysis.outputs_r22_passfailv2.root"]
svcMgr.THistSvc.MaxFileSize=-1 #speeds up jobs that output lots of histograms

# First create all the public tools to be used in the job
from AthenaCommon.AppMgr import ToolSvc
from TrigDecisionTool.TrigDecisionToolConf import Trig__TrigDecisionTool
import AthenaCommon.CfgMgr as CfgMgr

ToolSvc += CfgMgr.TrigConf__xAODConfigTool("xAODConfigTool")
ToolSvc += CfgMgr.Trig__TrigDecisionTool(
        "TrigDecisionTool",
        ConfigTool = ToolSvc.xAODConfigTool,
        TrigDecisionKey = "xTrigDecision",
        NavigationFormat = "TrigComposite") #NavigationFormat = "TrigComposite" 

from TrigEDMConfig.TriggerEDM import EDMLibraries
ToolSvc.TrigDecisionTool.Navigation.Dlls = [e for e in EDMLibraries if 'TPCnv' not in e]

ToolSvc += CfgMgr.TauAnalysisTools__TauTruthMatchingTool("TauTruthMatchingTool")

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
#alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )
alg = CfgMgr.MyxAODAnalysis(
    'AnalysisAlg',
    TrigDecisionTool = ToolSvc.TrigDecisionTool, 
    TauTruthMatchingTool = ToolSvc.TauTruthMatchingTool
    )

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the main alg sequence
athAlgSeq += alg

# limit the number of events (for testing purposes)
#theApp.EvtMax = 500

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")

