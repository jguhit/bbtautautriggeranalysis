#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/ToolHandle.h>

//ROOT include(s)
#include <TH1.h>
#include <TTree.h>
#include <vector>
#include <TSortedList.h>
#include <TObject.h>
#include <TLorentzVector.h>

//Tool include(s)
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigDecisionTool/ChainGroup.h"
#include "TrigDecisionTool/FeatureContainer.h"
#include "TrigDecisionTool/Feature.h"
#include "TrigDecisionTool/Conditions.h"
#include "TrigDecisionTool/DecisionAccess.h"
#include "TrigDecisionTool/ConfigurationAccess.h"
#include "TrigDecisionTool/Combination.h"
#include "TrigDecisionTool/TDTUtilities.h"

#include "TauAnalysisTools/TauTruthMatchingTool.h"

class MyxAODAnalysis : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
  ~MyxAODAnalysis();
  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

private:
  // Tool Configuration
  ToolHandle< Trig::TrigDecisionTool > m_trigDecTool;
  ToolHandle< TauAnalysisTools::TauTruthMatchingTool > m_T2MT;
  /*
  unsigned long long m_eventNumber = 0; // Event number
  std::vector<float> *m_actualBC = nullptr; 
  std::vector<float> *m_aveBC =  nullptr;
  */
  //L1 + HLT triggers
  std::vector<int> *L1_trig_pt = nullptr; 
  std::vector<int> *L1_trig_eta = nullptr; 
  std::vector<int> *HLT_trig_pt = nullptr; 
  std::vector<int> *HLT_trig_eta = nullptr;
  
  //perf chain
  /*
  std::vector<int> *Trig_perf_Opt1 = nullptr;
  std::vector<int> *Trig_perf_Opt2 = nullptr;
  */
  //TauJet Offline
  //Not Matched
  /*
  std::vector<int> *m_noff_taus = nullptr; 
  std::vector<TLorentzVector> *m_off_taus = nullptr;
  std::vector<bool> *m_off_tauIDvl = nullptr; 
  std::vector<bool> *m_off_tauIDl = nullptr; 
  std::vector<bool> *m_off_tauIDm = nullptr; 
  std::vector<bool> *m_off_tauIDt = nullptr;
  std::vector<size_t> *m_off_prong = nullptr; 
  std::vector<float> *m_off_rnn = nullptr;
  */

  //Matched
  std::vector<int> *m_noff_mtaus = nullptr; 
  std::vector<TLorentzVector> *m_off_mtaus = nullptr;
  std::vector<bool> *m_off_mtauIDvl = nullptr; 
  std::vector<bool> *m_off_mtauIDl = nullptr; 
  std::vector<bool> *m_off_mtauIDm = nullptr; 
  std::vector<bool> *m_off_mtauIDt = nullptr;
  std::vector<size_t> *m_off_mprong = nullptr; 
  std::vector<float> *m_off_mrnn = nullptr;

  /*
  //TauJet Online pT vars 
  std::vector<int> *m_non_mtaus = nullptr; 
  std::vector<TLorentzVector> *m_on_mtaus = nullptr;
  std::vector<bool> *m_on_mtauIDvl = nullptr; 
  std::vector<bool> *m_on_mtauIDl = nullptr; 
  std::vector<bool> *m_on_mtauIDm = nullptr; 
  std::vector<bool> *m_on_mtauIDt = nullptr;
  std::vector<size_t> *m_on_mprong = nullptr; 
  std::vector<float> *m_on_mrnn = nullptr;

  //TauJet Online Eta vars
  std::vector<int> *m_non_mtaus_eta = nullptr; 
  std::vector<TLorentzVector> *m_on_mtaus_eta = nullptr;
  std::vector<bool> *m_on_mtauIDvl_eta = nullptr; 
  std::vector<bool> *m_on_mtauIDl_eta = nullptr; 
  std::vector<bool> *m_on_mtauIDm_eta = nullptr; 
  std::vector<bool> *m_on_mtauIDt_eta = nullptr;
  std::vector<size_t> *m_on_mprong_eta = nullptr; 
  std::vector<float> *m_on_mrnn_eta = nullptr;
  */
  /*
  //Feature Container: passed and failed taus
  std::vector<int> *m_non_HLTpt = nullptr; 
  std::vector<TLorentzVector> *m_on_HLTpt = nullptr;
  std::vector<bool> *m_on_idvlHLTpt = nullptr; 
  std::vector<bool> *m_on_idlHLTpt = nullptr; 
  std::vector<bool> *m_on_idmHLTpt = nullptr; 
  std::vector<bool> *m_on_idtHLTpt = nullptr;
  std::vector<size_t> *m_on_prngHLTpt = nullptr; 
  std::vector<float> *m_on_rnnHLTpt = nullptr;

  std::vector<int> *m_non_HLTeta = nullptr; 
  std::vector<TLorentzVector> *m_on_HLTeta = nullptr;
  std::vector<bool> *m_on_idvlHLTeta = nullptr; 
  std::vector<bool> *m_on_idlHLTeta = nullptr; 
  std::vector<bool> *m_on_idmHLTeta = nullptr; 
  std::vector<bool> *m_on_idtHLTeta = nullptr;
  std::vector<size_t> *m_on_prngHLTeta = nullptr; 
  std::vector<float> *m_on_rnnHLTeta = nullptr;
  */
  /*
  //Feature Container: perf chain
  std::vector<int> *m_non_perfopt1 = nullptr; 
  std::vector<TLorentzVector> *m_on_perfopt1 = nullptr;
  std::vector<bool> *m_on_idvlperfopt1 = nullptr; 
  std::vector<bool> *m_on_idlperfopt1 = nullptr; 
  std::vector<bool> *m_on_idmperfopt1 = nullptr; 
  std::vector<bool> *m_on_idtperfopt1 = nullptr;
  std::vector<size_t> *m_on_prngperfopt1 = nullptr; 
  std::vector<float> *m_on_rnnperfopt1 = nullptr;
  */
  //TrigDefs::includeFailedDecisions
  std::vector<int> *m_non_HLTptfl = nullptr; 
  std::vector<TLorentzVector> *m_on_HLTptfl = nullptr;
  std::vector<bool> *m_on_idvlHLTptfl = nullptr; 
  std::vector<bool> *m_on_idlHLTptfl = nullptr; 
  std::vector<bool> *m_on_idmHLTptfl = nullptr; 
  std::vector<bool> *m_on_idtHLTptfl = nullptr;
  std::vector<size_t> *m_on_prngHLTptfl = nullptr; 
  std::vector<float> *m_on_rnnHLTptfl = nullptr;
  std::vector<bool> *m_on_HLTptflag = nullptr;

  std::vector<int> *m_non_HLTetafl = nullptr; 
  std::vector<TLorentzVector> *m_on_HLTetafl = nullptr;
  std::vector<bool> *m_on_idvlHLTetafl = nullptr; 
  std::vector<bool> *m_on_idlHLTetafl = nullptr; 
  std::vector<bool> *m_on_idmHLTetafl = nullptr; 
  std::vector<bool> *m_on_idtHLTetafl = nullptr;
  std::vector<size_t> *m_on_prngHLTetafl = nullptr; 
  std::vector<float> *m_on_rnnHLTetafl = nullptr;
  std::vector<bool> *m_on_HLTetaflag = nullptr;

  //HLT container (loop directly) TrigTauRecMerged
  std::vector<int> *m_non_TrigTRM = nullptr; 
  std::vector<TLorentzVector> *m_on_TrigTRM = nullptr;
  std::vector<bool> *m_on_idvlTrigTRM = nullptr; 
  std::vector<bool> *m_on_idlTrigTRM = nullptr; 
  std::vector<bool> *m_on_idmTrigTRM = nullptr; 
  std::vector<bool> *m_on_idtTrigTRM = nullptr;
  std::vector<size_t> *m_on_prngTrigTRM = nullptr; 
  std::vector<float> *m_on_rnnTrigTRM = nullptr;

};

#endif