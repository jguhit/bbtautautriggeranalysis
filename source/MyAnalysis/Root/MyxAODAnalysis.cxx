//#include <AsgTools/MessageCheck.h> //include for r21
#include <AsgTools/MessageCheckAsgTools.h> //include for r22
#include <MyAnalysis/MyxAODAnalysis.h>

//xAODs include(s)
#include <xAODEventInfo/EventInfo.h>
#include <xAODTau/TauJetContainer.h>
#include <xAODMuon/MuonContainer.h>
#include "xAODBase/IParticleContainer.h"
#include "xAODTrigger/TrigPassBits.h" 
#include "TrigConfHLTData/HLTTriggerElement.h"

#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigDecisionTool/ChainGroup.h"
#include "TrigDecisionTool/FeatureContainer.h"
#include "TrigDecisionTool/Feature.h"
#include "TrigDecisionTool/Conditions.h"
#include "TrigDecisionTool/DecisionAccess.h"
#include "TrigDecisionTool/ConfigurationAccess.h"
#include "TrigDecisionTool/Combination.h"
#include "TrigDecisionTool/TDTUtilities.h"

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
    m_trigDecTool( "Trig::TrigDecisionTool/TrigDecisionTool" ),
    m_T2MT( "TauAnalysisTools::TauTruthMatchingTool/TauTruthMatchingTool")
    //m_trigConfTool(new TrigConf::xAODConfigTool("xAODConfigTool")), 
    //m_trigDecTool(new Trig::TrigDecisionTool("TrigDecisionTool"))
{
  declareProperty("TrigDecisionTool", m_trigDecTool);
  declareProperty("TauTruthMatchingTool", m_T2MT);
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
}

MyxAODAnalysis :: ~MyxAODAnalysis (){
  //delete m_actualBC;
  //delete m_aveBC;
  
  /*
  delete m_noff_taus; 
  delete m_off_taus;
  delete m_off_tauIDvl;
  delete m_off_tauIDl; 
  delete m_off_tauIDm; 
  delete m_off_tauIDt;
  delete m_off_prong;
  delete m_off_rnn;
  */

  delete m_noff_mtaus;
  delete m_off_mtaus;
  delete m_off_mtauIDvl;
  delete m_off_mtauIDl; 
  delete m_off_mtauIDm; 
  delete m_off_mtauIDt;
  delete m_off_mprong;
  delete m_off_mrnn;

  /*
  delete m_non_mtaus;
  delete m_on_mtaus;
  delete m_on_mtauIDvl;
  delete m_on_mtauIDl; 
  delete m_on_mtauIDm; 
  delete m_on_mtauIDt;
  delete m_on_mprong;
  delete m_on_mrnn;
  */
  /*
  delete m_non_mtaus_eta;
  delete m_on_mtaus_eta;
  delete m_on_mtauIDvl_eta;
  delete m_on_mtauIDl_eta; 
  delete m_on_mtauIDm_eta; 
  delete m_on_mtauIDt_eta;
  delete m_on_mprong_eta;
  delete m_on_mrnn_eta;
  */
  /*
  delete m_non_HLTpt;
  delete m_on_HLTpt;
  delete m_on_idvlHLTpt;
  delete m_on_idlHLTpt; 
  delete m_on_idmHLTpt; 
  delete m_on_idtHLTpt;
  delete m_on_prngHLTpt;
  delete m_on_rnnHLTpt;
  */

  delete m_non_HLTptfl;
  delete m_on_HLTptfl;
  delete m_on_idvlHLTptfl;
  delete m_on_idlHLTptfl; 
  delete m_on_idmHLTptfl; 
  delete m_on_idtHLTptfl;
  delete m_on_prngHLTptfl;
  delete m_on_rnnHLTptfl;
  delete m_on_HLTptflag;

  delete m_non_HLTetafl;
  delete m_on_HLTetafl;
  delete m_on_idvlHLTetafl;
  delete m_on_idlHLTetafl; 
  delete m_on_idmHLTetafl; 
  delete m_on_idtHLTetafl;
  delete m_on_prngHLTetafl;
  delete m_on_rnnHLTetafl;
  delete m_on_HLTetaflag;

  /*
  delete m_non_HLTeta;
  delete m_on_HLTeta;
  delete m_on_idvlHLTeta;
  delete m_on_idlHLTeta; 
  delete m_on_idmHLTeta; 
  delete m_on_idtHLTeta;
  delete m_on_prngHLTeta;
  delete m_on_rnnHLTeta;
  */

  delete L1_trig_pt;
  delete L1_trig_eta;
  delete HLT_trig_pt;
  delete HLT_trig_eta;
  //delete Trig_perf_Opt1;
  //delete Trig_perf_Opt2;

  delete m_non_TrigTRM;
  delete m_on_TrigTRM; 
  delete m_on_idvlTrigTRM;
  delete m_on_idlTrigTRM; 
  delete m_on_idmTrigTRM; 
  delete m_on_idtTrigTRM; 
  delete m_on_prngTrigTRM;
  delete m_on_rnnTrigTRM;

  /*
  delete m_non_perfopt1;
  delete m_on_perfopt1;
  delete m_on_idvlperfopt1;
  delete m_on_idlperfopt1; 
  delete m_on_idmperfopt1; 
  delete m_on_idtperfopt1;
  delete m_on_prngperfopt1;
  delete m_on_rnnperfopt1;
  */

}

StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  //ANA_MSG_INFO ("in initialize");

  ANA_CHECK( m_trigDecTool.retrieve() );
  ANA_CHECK( m_T2MT.retrieve() );

  //Tree Setup
  ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));
  TTree* mytree = tree ("analysis");
  //EventInfo
  //mytree->Branch ("EventNumber", &m_eventNumber);
  //m_actualBC = new std::vector<float>();
  //mytree->Branch ("ActualBC", &m_actualBC);
  //m_aveBC = new std::vector<float>();
  //mytree->Branch ("AveBC", &m_aveBC);

  //Triggers
  L1_trig_pt = new std::vector<int>();
  mytree->Branch("L1_J25", &L1_trig_pt);
  L1_trig_eta = new std::vector<int>();
  mytree->Branch("L1_ETA25", &L1_trig_eta);
  HLT_trig_pt = new std::vector<int>();
  mytree->Branch("HLT_J25", &HLT_trig_pt);
  HLT_trig_eta = new std::vector<int>();
  mytree->Branch("HLT_ETA25", &HLT_trig_eta);
  /*
  Trig_perf_Opt1 = new std::vector<int>();
  mytree->Branch("Perf_Opt1", Trig_perf_Opt1);
  Trig_perf_Opt2 = new std::vector<int>();
  mytree->Branch("Perf_Opt2", Trig_perf_Opt2);
  */

  //TauJet
  /*
  m_noff_taus = new std::vector<int>();
  mytree->Branch("Noffline_Taus", &m_noff_taus);
  m_off_taus = new std::vector<TLorentzVector>();
  mytree->Branch ("OfflineTaus", &m_off_taus);
  m_off_tauIDvl = new std::vector<bool>();
  mytree->Branch ("Off_TauIDvl", &m_off_tauIDvl);
  m_off_tauIDl = new std::vector<bool>();
  mytree->Branch ("Off_TauIDl", &m_off_tauIDl);
  m_off_tauIDm = new std::vector<bool>();
  mytree->Branch ("Off_TauIDm", &m_off_tauIDm);
  m_off_tauIDt = new std::vector<bool>();
  mytree->Branch ("Off_TauIDt", &m_off_tauIDt);
  m_off_prong = new std::vector<size_t>();
  mytree->Branch ("Off_prong", &m_off_prong);
  m_off_rnn = new std::vector<float>(); 
  mytree->Branch ("Off_rnn", &m_off_rnn);
  */

  m_noff_mtaus = new std::vector<int>(); 
  mytree->Branch("Noffline_Matched_Taus", &m_noff_mtaus);
  m_off_mtaus = new std::vector<TLorentzVector>();
  mytree->Branch ("Offline_Matched_Taus", &m_off_mtaus);
  m_off_mtauIDvl = new std::vector<bool>();
  mytree->Branch ("Off_Matched_TauIDvl", &m_off_mtauIDvl);
  m_off_mtauIDl = new std::vector<bool>();
  mytree->Branch ("Off_Matched_TauIDl", &m_off_mtauIDl);
  m_off_mtauIDm = new std::vector<bool>();
  mytree->Branch ("Off_Matched_TauIDm", &m_off_mtauIDm);
  m_off_mtauIDt = new std::vector<bool>();
  mytree->Branch ("Off_Matched_TauIDt", &m_off_mtauIDt);
  m_off_mprong = new std::vector<size_t>();
  mytree->Branch ("Off_Matched_TauProng", &m_off_mprong);
  m_off_mrnn = new std::vector<float>(); 
  mytree->Branch ("Off_Matched_TauRNN", &m_off_mrnn);

  /*
  m_non_mtaus = new std::vector<int>(); 
  mytree->Branch("Nonline_Matched_Taus_pT", &m_non_mtaus);
  m_on_mtaus = new std::vector<TLorentzVector>();
  mytree->Branch ("Online_Matched_Taus_pT", &m_on_mtaus);
  m_on_mtauIDvl = new std::vector<bool>();
  mytree->Branch ("On_Matched_TauIDvl_pT", &m_on_mtauIDvl);
  m_on_mtauIDl = new std::vector<bool>();
  mytree->Branch ("On_Matched_TauIDl_pT", &m_on_mtauIDl);
  m_on_mtauIDm = new std::vector<bool>();
  mytree->Branch ("On_Matched_TauIDm_pT", &m_on_mtauIDm);
  m_on_mtauIDt = new std::vector<bool>();
  mytree->Branch ("On_Matched_TauIDt_pT", &m_on_mtauIDt);
  m_on_mprong = new std::vector<size_t>();
  mytree->Branch ("On_Matched_prong_pT", &m_on_mprong);
  m_on_mrnn = new std::vector<float>(); 
  mytree->Branch ("On_Matched_rnn_pT", &m_on_mrnn);
  */
  /*
  m_non_mtaus_eta = new std::vector<int>(); 
  mytree->Branch("Nonline_Matched_Taus_Eta", &m_non_mtaus_eta);
  m_on_mtaus_eta = new std::vector<TLorentzVector>();
  mytree->Branch ("Online_Matched_Taus_Eta", &m_on_mtaus_eta);
  m_on_mtauIDvl_eta = new std::vector<bool>();
  mytree->Branch ("On_Matched_TauIDvl_Eta", &m_on_mtauIDvl_eta);
  m_on_mtauIDl_eta = new std::vector<bool>();
  mytree->Branch ("On_Matched_TauIDl_Eta", &m_on_mtauIDl_eta);
  m_on_mtauIDm_eta = new std::vector<bool>();
  mytree->Branch ("On_Matched_TauIDm_Eta", &m_on_mtauIDm_eta);
  m_on_mtauIDt_eta = new std::vector<bool>();
  mytree->Branch ("On_Matched_TauIDt_Eta", &m_on_mtauIDt_eta);
  m_on_mprong_eta = new std::vector<size_t>();
  mytree->Branch ("On_Matched_prong_Eta", &m_on_mprong_eta);
  m_on_mrnn_eta = new std::vector<float>(); 
  mytree->Branch ("On_Matched_rnn_Eta", &m_on_mrnn_eta);
  */
  /*
  m_non_HLTpt = new std::vector<int>(); 
  mytree->Branch("Num_TrigMatched_Taus_HLTpt", &m_non_HLTpt);
  m_on_HLTpt = new std::vector<TLorentzVector>();
  mytree->Branch ("TrigMatched_Taus_HLTpt", &m_on_HLTpt);
  m_on_idvlHLTpt = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDvl_HLTpt", &m_on_idvlHLTpt);
  m_on_idlHLTpt = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDl_HLTpt", &m_on_idlHLTpt);
  m_on_idmHLTpt = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDm_HLTpt", &m_on_idmHLTpt);
  m_on_idtHLTpt = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDt_HLTpt", &m_on_idtHLTpt);
  m_on_prngHLTpt = new std::vector<size_t>();
  mytree->Branch ("TrigMatched_prong_HLTpt", &m_on_prngHLTpt);
  m_on_rnnHLTpt = new std::vector<float>(); 
  mytree->Branch ("TrigMatched_rnn_HLTpt", &m_on_rnnHLTpt);
  */

  m_non_HLTptfl = new std::vector<int>(); 
  mytree->Branch("Num_TrigMatched_Taus_HLTptfl", &m_non_HLTptfl);
  m_on_HLTptfl = new std::vector<TLorentzVector>();
  mytree->Branch ("TrigMatched_Taus_HLTptfl", &m_on_HLTptfl);
  m_on_idvlHLTptfl = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDvl_HLTptfl", &m_on_idvlHLTptfl); 
  m_on_idlHLTptfl = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDl_HLTptfl", &m_on_idlHLTptfl);
  m_on_idmHLTptfl = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDm_HLTptfl", &m_on_idmHLTptfl);
  m_on_idtHLTptfl = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDt_HLTptfl", &m_on_idtHLTptfl);
  m_on_prngHLTptfl = new std::vector<size_t>();
  mytree->Branch ("TrigMatched_prong_HLTptfl", &m_on_prngHLTptfl);
  m_on_rnnHLTptfl = new std::vector<float>(); 
  mytree->Branch ("TrigMatched_rnn_HLTptfl", &m_on_rnnHLTptfl);
  m_on_HLTptflag = new std::vector<bool>();
  mytree->Branch ("TrigMatched_HLTptflag", &m_on_HLTptflag); 

  m_non_HLTetafl = new std::vector<int>(); 
  mytree->Branch("Num_TrigMatched_Taus_HLTetafl", &m_non_HLTetafl);
  m_on_HLTetafl = new std::vector<TLorentzVector>();
  mytree->Branch ("TrigMatched_Taus_HLTetafl", &m_on_HLTetafl);
  m_on_idvlHLTetafl = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDvl_HLTetafl", &m_on_idvlHLTetafl);
  m_on_idlHLTetafl = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDl_HLTetafl", &m_on_idlHLTetafl);
  m_on_idmHLTetafl = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDm_HLTetafl", &m_on_idmHLTetafl);
  m_on_idtHLTetafl = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDt_HLTetafl", &m_on_idtHLTetafl);
  m_on_prngHLTetafl = new std::vector<size_t>();
  mytree->Branch ("TrigMatched_prong_HLTetafl", &m_on_prngHLTetafl);
  m_on_rnnHLTetafl = new std::vector<float>(); 
  mytree->Branch ("TrigMatched_rnn_HLTetafl", &m_on_rnnHLTetafl);
  m_on_HLTetaflag = new std::vector<bool>();
  mytree->Branch ("TrigMatched_HLTetaflag", &m_on_HLTetaflag);

  /*
  m_non_HLTeta = new std::vector<int>(); 
  mytree->Branch("Num_TrigMatched_Taus_HLTeta", &m_non_HLTeta);
  m_on_HLTeta = new std::vector<TLorentzVector>();
  mytree->Branch ("TrigMatched_Taus_HLTeta", &m_on_HLTeta);
  m_on_idvlHLTeta = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDvl_HLTeta", &m_on_idvlHLTeta);
  m_on_idlHLTeta = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDl_HLTeta", &m_on_idlHLTeta);
  m_on_idmHLTeta = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDm_HLTeta", &m_on_idmHLTeta);
  m_on_idtHLTeta = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDt_HLTeta", &m_on_idtHLTeta);
  m_on_prngHLTeta = new std::vector<size_t>();
  mytree->Branch ("TrigMatched_prong_HLTeta", &m_on_prngHLTeta);
  m_on_rnnHLTeta = new std::vector<float>(); 
  mytree->Branch ("TrigMatched_rnn_HLTeta", &m_on_rnnHLTeta);
  */
  /*
  m_non_perfopt1 = new std::vector<int>(); 
  mytree->Branch("Num_TrigMatched_Taus_perfopt1", &m_non_perfopt1);
  m_on_perfopt1 = new std::vector<TLorentzVector>();
  mytree->Branch ("TrigMatched_Taus_perfopt1", &m_on_perfopt1);
  m_on_idvlperfopt1 = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDvl_perfopt1", &m_on_idvlperfopt1);
  m_on_idlperfopt1 = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDl_perfopt1", &m_on_idlperfopt1);
  m_on_idmperfopt1 = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDm_perfopt1", &m_on_idmperfopt1);
  m_on_idtperfopt1 = new std::vector<bool>();
  mytree->Branch ("TrigMatched_TauIDt_perfopt1", &m_on_idtperfopt1);
  m_on_prngperfopt1 = new std::vector<size_t>();
  mytree->Branch ("TrigMatched_prong_perfopt1", &m_on_prngperfopt1);
  m_on_rnnperfopt1 = new std::vector<float>(); 
  mytree->Branch ("TrigMatched_rnn_perfopt1", &m_on_rnnperfopt1);
  */

  //HLT Container directly 
  m_non_TrigTRM = new std::vector<int>(); 
  mytree->Branch("Num_TrigTRM_Taus", &m_non_TrigTRM);
  m_on_TrigTRM = new std::vector<TLorentzVector>();
  mytree->Branch ("TrigTRM_Taus", &m_on_TrigTRM);
  m_on_idvlTrigTRM = new std::vector<bool>();
  mytree->Branch ("TrigTRM_TauIDvl", &m_on_idvlTrigTRM);
  m_on_idlTrigTRM = new std::vector<bool>();
  mytree->Branch ("TrigTRM_TauIDl", &m_on_idlTrigTRM);
  m_on_idmTrigTRM = new std::vector<bool>();
  mytree->Branch ("TrigTRM_TauIDm", &m_on_idmTrigTRM);
  m_on_idtTrigTRM = new std::vector<bool>();
  mytree->Branch ("TrigTRM_TauIDt", &m_on_idtTrigTRM);
  m_on_prngTrigTRM = new std::vector<size_t>();
  mytree->Branch ("TrigTRM_prong", &m_on_prngTrigTRM);
  m_on_rnnTrigTRM = new std::vector<float>(); 
  mytree->Branch ("TrigTRM_rnn", &m_on_rnnTrigTRM);

  return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  //ANA_MSG_INFO ("in execute");

  //L1 and HLT
  L1_trig_pt->clear();
  L1_trig_eta->clear();
  HLT_trig_pt->clear();
  HLT_trig_eta->clear();
  //Trig_perf_Opt1->clear();
  //Trig_perf_Opt2->clear();
  const std::string trigL1pT="L1_DR-TAU20ITAU12I-J25";
  const std::string trigL1eta="L1_TAU20IM_2TAU12IM_4J12.0ETA25";
  const std::string trigHLTpT="HLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_03dR30_L1DR-TAU20ITAU12I-J25";
  const std::string trigHLTptr22="HLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_03dRAB30_L1DR-TAU20ITAU12I-J25";
  const std::string trigHLTeta="HLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_L1TAU20IM_2TAU12IM_4J12.0ETA25";
  const std::string trigHLTetar22="HLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_03dRAB_L1TAU20IM_2TAU12IM_4J12p0ETA25";
  
  const std::string trigHLTptTau0="HLT_tau0_mediumRNN_tracktwoMVABDT_tau0_mediumRNN_tracktwoMVABDT_03dRAB30_L1DR-TAU20ITAU12I-J25";
  const std::string trigHLTetaTau0="HLT_tau0_mediumRNN_tracktwoMVABDT_tau0_mediumRNN_tracktwoMVABDT_03dRAB_L1TAU20IM_2TAU12IM_4J12p0ETA25";
  //PerfChains
  /*
  const std::string trigperf_opt1="HLT_tau35_perf_tracktwo_tau25_perf_tracktwo";
  const std::string trigperf_opt2="HLT_tau35_perf_tracktwo_tau25_perf_tracktwo_ditauM";
  const std::string trigperf_opt3="HLT_tau25_idperf_tracktwoMVA";
  const std::string trigperf_opt4="HLT_tau25_perf_tracktwoMVA";
  const std::string trigperf_opt5="HLT_tau0_perf_ptonly_L1TAU12";
  const std::string trigperf_opt6="HLT_tau0_perf_ptonly_L1TAU12IM";
  const std::string trigperf_opt7="HLT_tau0_perf_ptonly_L1TAU8";
  const std::string trigperf_opt8="HLT_tau0_idperf_tracktwoMVA_L1eTAU12";
  const std::string trigperf_opt9="HLT_tau25_idperf_tracktwo";
  const std::string trigperf_opt10="HLT_tau25_perf_tracktwo";
  const std::string trigperf_opt11="HLT_tau25_idperf_tracktwoEF";
  const std::string trigperf_opt12="HLT_tau25_perf_tracktwoEF";
  */

  auto cgL1pT = m_trigDecTool->isPassed(trigL1pT);
  auto cgL1eta = m_trigDecTool->isPassed(trigL1eta);
  auto cgHLTpT = m_trigDecTool->isPassed(trigHLTptr22);
  auto cgHLTeta = m_trigDecTool->isPassed(trigHLTetar22);
  /*
  auto cgopt1perf = m_trigDecTool->isPassed(trigperf_opt1);
  auto cgopt2perf = m_trigDecTool->isPassed(trigperf_opt2);
  */

  if (!cgL1pT) {
    L1_trig_pt->push_back(0);
  } else {
    L1_trig_pt->push_back(1);
  }
  if (!cgL1eta) {
    L1_trig_eta->push_back(0);
  } else {
    L1_trig_eta->push_back(1);
  }
  if (!cgHLTpT) {
    HLT_trig_pt->push_back(0);
  } else {
    HLT_trig_pt->push_back(1);
  }
  if (!cgHLTeta) {
    HLT_trig_eta->push_back(0);
  } else {
    HLT_trig_eta->push_back(1);
  }
  /*
  if (!cgopt1perf) {
    Trig_perf_Opt1->push_back(0);
  } else {
    Trig_perf_Opt1->push_back(1);
  }
  if (!cgopt2perf) {
    Trig_perf_Opt2->push_back(0);
  } else {
    Trig_perf_Opt2->push_back(1);
  }
  */
  
  //TrigDefs::includeFailedDecisions pT trigger
  std::string tauContainerName = "HLT_TrigTauRecMerged_MVA";
  auto vec = m_trigDecTool->features<xAOD::TauJetContainer>(trigHLTptTau0, TrigDefs::includeFailedDecisions ,tauContainerName); //TrigDefs::includeFailedDecisions TrigDefs::Physics
  m_on_HLTptfl->clear();
  m_on_idvlHLTptfl->clear();
  m_on_idlHLTptfl->clear();
  m_on_idmHLTptfl->clear();
  m_on_idtHLTptfl->clear();
  m_on_prngHLTptfl->clear();
  m_on_rnnHLTptfl->clear();
  m_non_HLTptfl->clear();
  m_on_HLTptflag->clear();
  int countonhltptfl = 0;
  //ANA_MSG_INFO("Before loop");
  for (auto& featLinkInfo : vec){
    const auto *feat = *(featLinkInfo.link);
    if(!feat) continue; 
    //ANA_MSG_INFO("Inside Loop");
    //ANA_MSG_INFO("Tau feat pT: "<< feat->pt() * 0.001);
    if (feat->pt() * 0.001 < 10.) continue;
    TLorentzVector ontauhltptfl; 
    ontauhltptfl.SetPtEtaPhiM(feat->pt() * 0.001, feat->eta(), feat->phi(), 0);
    m_on_HLTptfl->push_back(ontauhltptfl);
    m_on_rnnHLTptfl->push_back(feat->discriminant(xAOD::TauJetParameters::TauID::RNNJetScoreSigTrans));
    m_on_prngHLTptfl->push_back(feat->nTracks());
    m_on_idvlHLTptfl->push_back(feat->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigVeryLoose));
    m_on_idlHLTptfl->push_back(feat->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigLoose));
    m_on_idmHLTptfl->push_back(feat->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigMedium));
    m_on_idtHLTptfl->push_back(feat->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigTight));
    if (feat->pt() * 0.001 < 280.) {
      m_on_HLTptflag->push_back(feat->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigMedium));
    } else if ( feat->pt() * 0.001 > 280.  &&  feat->pt() * 0.001 < 440.) {
      m_on_HLTptflag->push_back(feat->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigLoose));
    } else if (feat->pt() * 0.001 > 440){
      m_on_HLTptflag->push_back(1);
    }
    countonhltptfl ++;
  }
  //Sorting 
  TLorentzVector ontmpmtau_hltptfl;
  bool ontmpmtauIDvl_hltptfl, ontmpmtauIDl_hltptfl, ontmpmtauIDm_hltptfl, ontmpmtauIDt_hltptfl, ontmp_hltptflag; 
  size_t ontmp_mprong_hltptfl; 
  float ontmp_mrnn_hltptfl;
  for ( int i =0 ; i < (int)m_on_HLTptfl->size() - 1; i++){
    for (int j = i + 1; j < (int)m_on_HLTptfl->size(); j++){
      if (m_on_HLTptfl->at(i).Pt()<m_on_HLTptfl->at(j).Pt()){
        ontmpmtau_hltptfl = m_on_HLTptfl->at(i);
        m_on_HLTptfl->at(i) = m_on_HLTptfl->at(j);
        m_on_HLTptfl->at(j) = ontmpmtau_hltptfl; 

        ontmpmtauIDvl_hltptfl = m_on_idvlHLTptfl->at(i);
        m_on_idvlHLTptfl->at(i) = m_on_idvlHLTptfl->at(j);
        m_on_idvlHLTptfl->at(j) = ontmpmtauIDvl_hltptfl;

        ontmpmtauIDl_hltptfl = m_on_idlHLTptfl->at(i);
        m_on_idlHLTptfl->at(i) = m_on_idlHLTptfl->at(j);
        m_on_idlHLTptfl->at(j) = ontmpmtauIDl_hltptfl;

        ontmpmtauIDm_hltptfl = m_on_idmHLTptfl->at(i);
        m_on_idmHLTptfl->at(i) = m_on_idmHLTptfl->at(j);
        m_on_idmHLTptfl->at(j) = ontmpmtauIDm_hltptfl;

        ontmpmtauIDt_hltptfl = m_on_idtHLTptfl->at(i);
        m_on_idtHLTptfl->at(i) = m_on_idtHLTptfl->at(j);
        m_on_idtHLTptfl->at(j) = ontmpmtauIDt_hltptfl;

        ontmp_mprong_hltptfl = m_on_prngHLTptfl->at(i);
        m_on_prngHLTptfl->at(i) = m_on_prngHLTptfl->at(j);
        m_on_prngHLTptfl->at(j) = ontmp_mprong_hltptfl;

        ontmp_mrnn_hltptfl = m_on_rnnHLTptfl->at(i);
        m_on_rnnHLTptfl->at(i) = m_on_rnnHLTptfl->at(j);
        m_on_rnnHLTptfl->at(j) = ontmp_mrnn_hltptfl;

        ontmp_hltptflag = m_on_HLTptflag->at(i);
        m_on_HLTptflag->at(i) = m_on_HLTptflag->at(j);
        m_on_HLTptflag->at(j) = ontmp_hltptflag;
      }
    }
  }
  m_non_HLTptfl->push_back(countonhltptfl);
  
  //TrigDefs::includeFailedDecisions eta trigger
  auto veceta = m_trigDecTool->features<xAOD::TauJetContainer>(trigHLTetaTau0, TrigDefs::includeFailedDecisions ,tauContainerName); //TrigDefs::includeFailedDecisions TrigDefs::Physics
  m_on_HLTetafl->clear();
  m_on_idvlHLTetafl->clear();
  m_on_idlHLTetafl->clear();
  m_on_idmHLTetafl->clear();
  m_on_idtHLTetafl->clear();
  m_on_prngHLTetafl->clear();
  m_on_rnnHLTetafl->clear();
  m_non_HLTetafl->clear();
  m_on_HLTetaflag->clear();
  int countonhltetafl = 0;
  //ANA_MSG_INFO("Before loop");
  for (auto& featLinkInfo_eta : veceta){
    const auto *feat_eta = *(featLinkInfo_eta.link);
    if(!feat_eta) continue; 
    //ANA_MSG_INFO("Inside Loop");
    //ANA_MSG_INFO("Tau feat pT: "<< feat_eta->pt() * 0.001);
    if (feat_eta->pt() * 0.001 < 10.) continue;
    TLorentzVector ontauhltetafl; 
    ontauhltetafl.SetPtEtaPhiM(feat_eta->pt() * 0.001, feat_eta->eta(), feat_eta->phi(), 0);
    m_on_HLTetafl->push_back(ontauhltetafl);
    m_on_prngHLTetafl->push_back(feat_eta->nTracks());
    m_on_rnnHLTetafl->push_back(feat_eta->discriminant(xAOD::TauJetParameters::TauID::RNNJetScoreSigTrans)); 
    m_on_idvlHLTetafl->push_back(feat_eta->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigVeryLoose));
    m_on_idlHLTetafl->push_back(feat_eta->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigLoose));
    m_on_idmHLTetafl->push_back(feat_eta->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigMedium));
    m_on_idtHLTetafl->push_back(feat_eta->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigTight));
    if (feat_eta->pt() * 0.001 < 280.) {
      m_on_HLTetaflag->push_back(feat_eta->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigMedium));
    } else if ( feat_eta->pt() * 0.001 > 280.  &&  feat_eta->pt() * 0.001 < 440.) {
      m_on_HLTetaflag->push_back(feat_eta->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigLoose));
    } else if (feat_eta->pt() * 0.001 > 440){
      m_on_HLTetaflag->push_back(1);
    }
    countonhltetafl ++;
  }
  //Sorting 
  TLorentzVector ontmpmtau_hltetafl;
  bool ontmpmtauIDvl_hltetafl, ontmpmtauIDl_hltetafl, ontmpmtauIDm_hltetafl, ontmpmtauIDt_hltetafl, ontmp_hltetaflag;
  size_t ontmp_mprong_hltetafl; 
  float ontmp_mrnn_hltetafl;
  for ( int i =0 ; i < (int)m_on_HLTetafl->size() - 1; i++){
    for (int j = i + 1; j < (int)m_on_HLTetafl->size(); j++){
      if (m_on_HLTetafl->at(i).Pt()<m_on_HLTetafl->at(j).Pt()){
        ontmpmtau_hltetafl = m_on_HLTetafl->at(i);
        m_on_HLTetafl->at(i) = m_on_HLTetafl->at(j);
        m_on_HLTetafl->at(j) = ontmpmtau_hltetafl; 

        ontmpmtauIDvl_hltetafl = m_on_idvlHLTetafl->at(i);
        m_on_idvlHLTetafl->at(i) = m_on_idvlHLTetafl->at(j);
        m_on_idvlHLTetafl->at(j) = ontmpmtauIDvl_hltetafl;

        ontmpmtauIDl_hltetafl = m_on_idlHLTetafl->at(i);
        m_on_idlHLTetafl->at(i) = m_on_idlHLTetafl->at(j);
        m_on_idlHLTetafl->at(j) = ontmpmtauIDl_hltetafl;

        ontmpmtauIDm_hltetafl = m_on_idmHLTetafl->at(i);
        m_on_idmHLTetafl->at(i) = m_on_idmHLTetafl->at(j);
        m_on_idmHLTetafl->at(j) = ontmpmtauIDm_hltetafl;

        ontmpmtauIDt_hltetafl = m_on_idtHLTetafl->at(i);
        m_on_idtHLTetafl->at(i) = m_on_idtHLTetafl->at(j);
        m_on_idtHLTetafl->at(j) = ontmpmtauIDt_hltetafl;

        ontmp_mprong_hltetafl = m_on_prngHLTetafl->at(i);
        m_on_prngHLTetafl->at(i) = m_on_prngHLTetafl->at(j);
        m_on_prngHLTetafl->at(j) = ontmp_mprong_hltetafl;

        ontmp_mrnn_hltetafl = m_on_rnnHLTetafl->at(i);
        m_on_rnnHLTetafl->at(i) = m_on_rnnHLTetafl->at(j);
        m_on_rnnHLTetafl->at(j) = ontmp_mrnn_hltetafl;

        ontmp_hltetaflag = m_on_HLTetaflag->at(i);
        m_on_HLTetaflag->at(i) = m_on_HLTetaflag->at(j);
        m_on_HLTetaflag->at(j) = ontmp_hltetaflag;
      }
    }
  }
  m_non_HLTetafl->push_back(countonhltetafl);
  
  /*
  //Online taus perf trigger chain 
  Trig::FeatureContainer fcperf1 = m_trigDecTool->features( trigperf_opt4, TrigDefs::alsoDeactivateTEs ); //trigperf_opt1
  std::vector<Trig::Feature<xAOD::TauJetContainer> > vecperf1 = fcperf1.containerFeature<xAOD::TauJetContainer>("TrigTauRecMerged"); //TrigTauRecMerged
  m_on_perfopt1->clear();
  m_on_idvlperfopt1->clear();
  m_on_idlperfopt1->clear();
  m_on_idmperfopt1->clear();
  m_on_idtperfopt1->clear();
  m_on_prngperfopt1->clear();
  m_on_rnnperfopt1->clear();
  m_non_perfopt1->clear();
  int countonperfopt1 = 0;
  for( const Trig::Feature<xAOD::TauJetContainer>& taufeat_perf1 : vecperf1) {
    std::string labelperf1;
    TrigConf::HLTTriggerElement::getLabel (taufeat_perf1.te()->getId(), labelperf1);
    ANA_MSG_INFO("... Tau label perf1: " << taufeat_perf1.label() << " Active " 
            << taufeat_perf1.te()->getActiveState() 
            << " Id " << taufeat_perf1.te()->getId() 
            << " label " << labelperf1);
    
    // Get a pointer to the container from the TriggerElement feature 
    auto *cont_perf1=taufeat_perf1.cptr(); // const xAOD::TauJetContainer
    if(!cont_perf1) {
        ANA_MSG_WARNING("TauJetContainer is Null");
        continue;
    }
    if (cont_perf1->empty()) continue;
    
    // Retrieve the PassBits
    // Objects in containers associated to a TriggerElement are marked as passed/failed

    const xAOD::TrigPassBits *bits_perf1=(m_trigDecTool->ancestor<xAOD::TrigPassBits>(taufeat_perf1.te())).cptr();
    if (!bits_perf1)
      ANA_MSG_WARNING("TrigPassBits null");

    for (auto *tperf1 : *cont_perf1){ //const xAOD::TauJet
      if (!tperf1){
        ANA_MSG_WARNING("TauJet Object is null");
        continue;
      }
      if (bits_perf1){
        //ANA_MSG_INFO("Tau Selected " << bits->isPassing(t, cont) << "pT: " <<  t->pt()*0.001 << "eta: " << t->eta());
        if (tperf1->pt() * 0.001 < 10.) continue;
        TLorentzVector ontauperf1; 
        ontauperf1.SetPtEtaPhiM(tperf1->pt() * 0.001, tperf1->eta(), tperf1->phi(), 0);
        m_on_perfopt1->push_back(ontauperf1);
        m_on_idvlperfopt1->push_back(tperf1->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigVeryLoose));
        m_on_idlperfopt1->push_back(tperf1->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigLoose));
        m_on_idmperfopt1->push_back(tperf1->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigMedium));
        m_on_idtperfopt1->push_back(tperf1->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigTight));
        m_on_prngperfopt1->push_back(tperf1->nTracks());
        m_on_rnnperfopt1->push_back(tperf1->discriminant(xAOD::TauJetParameters::TauID::RNNJetScoreSigTrans)); 
        countonperfopt1 ++;
      }
    }
  }
  // Sorting Loop both pass and fail pt trigger 
  TLorentzVector ontmpmtau_perfopt1;
  bool ontmpmtauIDvl_perfopt1, ontmpmtauIDl_perfopt1, ontmpmtauIDm_perfopt1, ontmpmtauIDt_perfopt1; 
  size_t ontmp_mprong_perfopt1; 
  float ontmp_mrnn_perfopt1;
  for ( int i =0 ; i < (int)m_on_perfopt1->size() - 1; i++){
    for (int j = i + 1; j < (int)m_on_perfopt1->size(); j++){
      if (m_on_perfopt1->at(i).Pt()<m_on_perfopt1->at(j).Pt()){
        ontmpmtau_perfopt1 = m_on_perfopt1->at(i);
        m_on_perfopt1->at(i) = m_on_perfopt1->at(j);
        m_on_perfopt1->at(j) = ontmpmtau_perfopt1; 

        ontmpmtauIDvl_perfopt1 = m_on_idvlperfopt1->at(i);
        m_on_idvlperfopt1->at(i) = m_on_idvlperfopt1->at(j);
        m_on_idvlperfopt1->at(j) = ontmpmtauIDvl_perfopt1;

        ontmpmtauIDl_perfopt1 = m_on_idlperfopt1->at(i);
        m_on_idlperfopt1->at(i) = m_on_idlperfopt1->at(j);
        m_on_idlperfopt1->at(j) = ontmpmtauIDl_perfopt1;

        ontmpmtauIDm_perfopt1 = m_on_idmperfopt1->at(i);
        m_on_idmperfopt1->at(i) = m_on_idmperfopt1->at(j);
        m_on_idmperfopt1->at(j) = ontmpmtauIDm_perfopt1;

        ontmpmtauIDt_perfopt1 = m_on_idtperfopt1->at(i);
        m_on_idtperfopt1->at(i) = m_on_idtperfopt1->at(j);
        m_on_idtperfopt1->at(j) = ontmpmtauIDt_perfopt1;

        ontmp_mprong_perfopt1 = m_on_prngperfopt1->at(i);
        m_on_prngperfopt1->at(i) = m_on_prngperfopt1->at(j);
        m_on_prngperfopt1->at(j) = ontmp_mprong_perfopt1;

        ontmp_mrnn_perfopt1 = m_on_rnnperfopt1->at(i);
        m_on_rnnperfopt1->at(i) = m_on_rnnperfopt1->at(j);
        m_on_rnnperfopt1->at(j) = ontmp_mrnn_perfopt1;
      }
    }
  }
  m_non_perfopt1->push_back(countonperfopt1);
  */
  /*
  //Online Taus (both pass and fail) pT trigger 
  Trig::FeatureContainer fcpt = m_trigDecTool->features( trigHLTptr22, TrigDefs::Physics ); //r21: trigHLTpt // TrigDefs::Physics //TrigDefs::alsoDeactivateTEs 
  std::vector<Trig::Feature<xAOD::TauJetContainer> > hltpt_vec = fcpt.get<xAOD::TauJetContainer>("HLT_TrigTauRecMerged_MVA"); //r21: TrigTauRecMerged
  //auto hltpt_vec = m_trigDecTool->features<xAOD::TauJetContainer>(trigHLTpT, TrigDefs::includeFailedDecisions,"TrigTauRecMerged")
  m_on_HLTpt->clear();
  m_on_idvlHLTpt->clear();
  m_on_idlHLTpt->clear();
  m_on_idmHLTpt->clear();
  m_on_idtHLTpt->clear();
  m_on_prngHLTpt->clear();
  m_on_rnnHLTpt->clear();
  m_non_HLTpt->clear();
  int countonHLTpt = 0; 
  for( const Trig::Feature<xAOD::TauJetContainer>& taufeatpt : hltpt_vec ) {
    std::string label;
    TrigConf::HLTTriggerElement::getLabel (taufeatpt.te()->getId(), label );
    ANA_MSG_INFO("... Tau pT label: " << taufeatpt.label() << " Active " 
            << taufeatpt.te()->getActiveState() 
            << " Id " << taufeatpt.te()->getId() 
            << " label " << label);
    
    // Get a pointer to the container from the TriggerElement feature 
    auto *cont=taufeatpt.cptr(); // const xAOD::TauJetContainer
    if(!cont) {
        ANA_MSG_WARNING("TauJetContainer is Null");
        continue;
    }
    if (cont->empty()) continue;
    
    // Retrieve the PassBits
    // Objects in containers associated to a TriggerElement are marked as passed/failed

    const xAOD::TrigPassBits *bits=(m_trigDecTool->ancestor<xAOD::TrigPassBits>(taufeatpt.te())).cptr();
    if (!bits)
      ANA_MSG_WARNING("TrigPassBits null");

    for (auto *t : *cont){ //const xAOD::TauJet
      if (!t){
        ANA_MSG_WARNING("TauJet Object is null");
        continue;
      }
      if (bits){
        //ANA_MSG_INFO("Tau Selected " << bits->isPassing(t, cont) << "pT: " <<  t->pt()*0.001 << "eta: " << t->eta());
        if (t->pt() * 0.001 < 10.) continue;
        TLorentzVector ontauHLTpt; 
        ontauHLTpt.SetPtEtaPhiM(t->pt() * 0.001, t->eta(), t->phi(), 0);
        m_on_HLTpt->push_back(ontauHLTpt);
        m_on_idvlHLTpt->push_back(t->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigVeryLoose));
        m_on_idlHLTpt->push_back(t->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigLoose));
        m_on_idmHLTpt->push_back(t->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigMedium));
        m_on_idtHLTpt->push_back(t->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigTight));
        m_on_prngHLTpt->push_back(t->nTracks());
        m_on_rnnHLTpt->push_back(t->discriminant(xAOD::TauJetParameters::TauID::RNNJetScoreSigTrans)); 
        countonHLTpt ++;
      }
    }
  }
  // Sorting Loop both pass and fail pt trigger 
  TLorentzVector ontmpmtau_hltpt;
  bool ontmpmtauIDvl_hltpt, ontmpmtauIDl_hltpt, ontmpmtauIDm_hltpt, ontmpmtauIDt_hltpt; 
  size_t ontmp_mprong_hltpt; 
  float ontmp_mrnn_hltpt;
  for ( int i =0 ; i < (int)m_on_HLTpt->size() - 1; i++){
    for (int j = i + 1; j < (int)m_on_HLTpt->size(); j++){
      if (m_on_HLTpt->at(i).Pt()<m_on_HLTpt->at(j).Pt()){
        ontmpmtau_hltpt = m_on_HLTpt->at(i);
        m_on_HLTpt->at(i) = m_on_HLTpt->at(j);
        m_on_HLTpt->at(j) = ontmpmtau_hltpt; 

        ontmpmtauIDvl_hltpt = m_on_idvlHLTpt->at(i);
        m_on_idvlHLTpt->at(i) = m_on_idvlHLTpt->at(j);
        m_on_idvlHLTpt->at(j) = ontmpmtauIDvl_hltpt;

        ontmpmtauIDl_hltpt = m_on_idlHLTpt->at(i);
        m_on_idlHLTpt->at(i) = m_on_idlHLTpt->at(j);
        m_on_idlHLTpt->at(j) = ontmpmtauIDl_hltpt;

        ontmpmtauIDm_hltpt = m_on_idmHLTpt->at(i);
        m_on_idmHLTpt->at(i) = m_on_idmHLTpt->at(j);
        m_on_idmHLTpt->at(j) = ontmpmtauIDm_hltpt;

        ontmpmtauIDt_hltpt = m_on_idtHLTpt->at(i);
        m_on_idtHLTpt->at(i) = m_on_idtHLTpt->at(j);
        m_on_idtHLTpt->at(j) = ontmpmtauIDt_hltpt;

        ontmp_mprong_hltpt = m_on_prngHLTpt->at(i);
        m_on_prngHLTpt->at(i) = m_on_prngHLTpt->at(j);
        m_on_prngHLTpt->at(j) = ontmp_mprong_hltpt;

        ontmp_mrnn_hltpt = m_on_rnnHLTpt->at(i);
        m_on_rnnHLTpt->at(i) = m_on_rnnHLTpt->at(j);
        m_on_rnnHLTpt->at(j) = ontmp_mrnn_hltpt;
      }
    }
  }
  m_non_HLTpt->push_back(countonHLTpt);

  //Online taus (both pass and fail) eta trigger
  //Trig::FeatureContainer fceta = m_trigDecTool->features( trigHLTetar22, TrigDefs::Physics ); //r21: trigHLTetar22
  //std::vector<Trig::Feature<xAOD::TauJetContainer> > hlteta_vec = fceta.get<xAOD::TauJetContainer>("HLT_TrigTauRecMerged_MVA"); //r21: TrigTauRecMerged
  auto veceta_pass = m_trigDecTool->features<xAOD::TauJetContainer>(trigHLTetar22, TrigDefs::Physics, tauContainerName);
  m_on_HLTeta->clear();
  m_on_idvlHLTeta->clear();
  m_on_idlHLTeta->clear();
  m_on_idmHLTeta->clear();
  m_on_idtHLTeta->clear();
  m_on_prngHLTeta->clear();
  m_on_rnnHLTeta->clear();
  m_non_HLTeta->clear();
  int countonHLTeta = 0;
  */
  /*
  for( const Trig::Feature<xAOD::TauJetContainer>& taufeateta : hlteta_vec){
    std::string labeleta; 
    TrigConf::HLTTriggerElement::getLabel (taufeateta.te()->getId(), labeleta );
    ANA_MSG_INFO("... Tau Eta label: " << taufeateta.label() << " Active " 
            << taufeateta.te()->getActiveState() 
            << " Id " << taufeateta.te()->getId() 
            << " label " << labeleta);
    
    // Get a pointer to the container from the TriggerElement feature
    const xAOD::TauJetContainer *cont_eta=taufeateta.cptr();
    if(!cont_eta) {
        ANA_MSG_WARNING("TauJetContainer is Null");
        continue;
    }
    if (cont_eta->empty()) continue;

    // Retrieve the PassBits
    // Objects in containers associated to a TriggerElement are marked as passed/failed

    const xAOD::TrigPassBits *bits_eta=(m_trigDecTool->ancestor<xAOD::TrigPassBits>(taufeateta.te())).cptr();
    if (!bits_eta)
      ANA_MSG_WARNING("TrigPassBits null");

    for (const xAOD::TauJet *t_eta : *cont_eta){
      if (!t_eta){
        ANA_MSG_WARNING("Tau Object is null");
        continue;
      }
      if (bits_eta){
        //ANA_MSG_INFO("Tau Selected " << bits_eta->isPassing(t_eta, cont_eta) << "pT: " << t_eta->pt()*0.001 << "eta: " << t_eta->eta());
        if (t_eta->pt() * 0.001 < 10.) continue;
        TLorentzVector ontauHLTeta; 
        ontauHLTeta.SetPtEtaPhiM(t_eta->pt() * 0.001, t_eta->eta(), t_eta->phi(), 0);
        m_on_HLTeta->push_back(ontauHLTeta);
        m_on_idvlHLTeta->push_back(t_eta->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigVeryLoose));
        m_on_idlHLTeta->push_back(t_eta->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigLoose));
        m_on_idmHLTeta->push_back(t_eta->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigMedium));
        m_on_idtHLTeta->push_back(t_eta->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigTight));
        m_on_prngHLTeta->push_back(t_eta->nTracks());
        m_on_rnnHLTeta->push_back(t_eta->discriminant(xAOD::TauJetParameters::TauID::RNNJetScoreSigTrans)); 
        countonHLTeta ++;
      }
    }
  }
  */
  /*
  for (auto& featLinkInfo_etap : veceta){
    const auto *feat_eta_pass = *(featLinkInfo_etap.link);
    if(!feat_eta_pass) continue; 
    ANA_MSG_INFO("Inside Passed Loop");
    ANA_MSG_INFO("Tau feat Passed eta: "<< feat_eta_pass->pt() * 0.001);
    if (feat_eta_pass->pt() * 0.001 < 10.) continue;
    TLorentzVector ontauHLTeta; 
    ontauHLTeta.SetPtEtaPhiM(feat_eta_pass->pt() * 0.001, feat_eta_pass->eta(), feat_eta_pass->phi(), 0);
    m_on_HLTeta->push_back(ontauHLTeta);
    m_on_idvlHLTeta->push_back(feat_eta_pass->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigVeryLoose));
    m_on_idlHLTeta->push_back(feat_eta_pass->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigLoose));
    m_on_idmHLTeta->push_back(feat_eta_pass->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigMedium));
    m_on_idtHLTeta->push_back(feat_eta_pass->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigTight));
    m_on_prngHLTeta->push_back(feat_eta_pass->nTracks());
    m_on_rnnHLTeta->push_back(feat_eta_pass->discriminant(xAOD::TauJetParameters::TauID::RNNJetScoreSigTrans)); 
    countonHLTeta ++;
  }
  // Sorting Loop both pass and fail HLT trigger
  TLorentzVector ontmpmtau_hlteta;
  bool ontmpmtauIDvl_hlteta, ontmpmtauIDl_hlteta, ontmpmtauIDm_hlteta, ontmpmtauIDt_hlteta; 
  size_t ontmp_mprong_hlteta; 
  float ontmp_mrnn_hlteta;
  for ( int i =0 ; i < (int)m_on_HLTeta->size() - 1; i++){
    for (int j = i + 1; j < (int)m_on_HLTeta->size(); j++){
      if (m_on_HLTeta->at(i).Pt()<m_on_HLTeta->at(j).Pt()){
        ontmpmtau_hlteta = m_on_HLTeta->at(i);
        m_on_HLTeta->at(i) = m_on_HLTeta->at(j);
        m_on_HLTeta->at(j) = ontmpmtau_hlteta; 

        ontmpmtauIDvl_hlteta = m_on_idvlHLTeta->at(i);
        m_on_idvlHLTeta->at(i) = m_on_idvlHLTeta->at(j);
        m_on_idvlHLTeta->at(j) = ontmpmtauIDvl_hlteta;

        ontmpmtauIDl_hlteta = m_on_idlHLTeta->at(i);
        m_on_idlHLTeta->at(i) = m_on_idlHLTeta->at(j);
        m_on_idlHLTeta->at(j) = ontmpmtauIDl_hlteta;

        ontmpmtauIDm_hlteta = m_on_idmHLTeta->at(i);
        m_on_idmHLTeta->at(i) = m_on_idmHLTeta->at(j);
        m_on_idmHLTeta->at(j) = ontmpmtauIDm_hlteta;

        ontmpmtauIDt_hlteta = m_on_idtHLTeta->at(i);
        m_on_idtHLTeta->at(i) = m_on_idtHLTeta->at(j);
        m_on_idtHLTeta->at(j) = ontmpmtauIDt_hlteta;

        ontmp_mprong_hlteta = m_on_prngHLTeta->at(i);
        m_on_prngHLTeta->at(i) = m_on_prngHLTeta->at(j);
        m_on_prngHLTeta->at(j) = ontmp_mprong_hlteta;

        ontmp_mrnn_hlteta = m_on_rnnHLTeta->at(i);
        m_on_rnnHLTeta->at(i) = m_on_rnnHLTeta->at(j);
        m_on_rnnHLTeta->at(j) = ontmp_mrnn_hlteta;
      }
    }
  }
  m_non_HLTeta->push_back(countonHLTeta);
  */
  /*
  //Online Taus (only passed)
  // Retrieve the features of an object for a specific trigger (pT trigger)
  //auto cg = m_trigDecTool->getChainGroup("HLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_03dR30_L1DR-TAU20ITAU12I-J25");
  //auto cg = m_trigDecTool->getChainGroup(trigHLTptr22);
  auto cg = m_trigDecTool->getChainGroup(trigHLTptr22);
  auto fc = cg->features();
  auto tauFeatureContainers = fc.get<xAOD::TauJetContainer>(tauContainerName);
  //auto tauFeatureContainers = fc.containerFeature<xAOD::TauJetContainer>(tauContainerName);
  m_on_mtaus->clear();
  m_on_mtauIDvl->clear();
  m_on_mtauIDl->clear();
  m_on_mtauIDm->clear();
  m_on_mtauIDt->clear();
  m_on_mprong->clear();
  m_on_mrnn->clear();
  m_non_mtaus->clear();
  int counton = 0; 
  for (auto taucont : tauFeatureContainers){
    for (auto tau : *taucont.cptr()){
      if (tau->pt() * 0.001 < 10.) continue;
      ANA_MSG_INFO("Tau Only Passed pT: "<< tau->pt() * 0.001);
      TLorentzVector ontau; 
      ontau.SetPtEtaPhiM(tau->pt() * 0.001, tau->eta(), tau->phi(), 0);
      m_on_mtaus->push_back(ontau);
      m_on_mtauIDvl->push_back(tau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigVeryLoose));
      m_on_mtauIDl->push_back(tau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigLoose));
      m_on_mtauIDm->push_back(tau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigMedium));
      m_on_mtauIDt->push_back(tau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigTight));
      m_on_mprong->push_back(tau->nTracks());
      m_on_mrnn->push_back(tau->discriminant(xAOD::TauJetParameters::TauID::RNNJetScoreSigTrans)); 
      counton ++; 
    }
  }
  // Sorting Loop
  TLorentzVector ontmpmtau;
  bool ontmpmtauIDvl, ontmpmtauIDl, ontmpmtauIDm, ontmpmtauIDt; 
  size_t ontmp_mprong; 
  float ontmp_mrnn;
  for ( int i =0 ; i < (int)m_on_mtaus->size() - 1; i++){
    for (int j = i + 1; j < (int)m_on_mtaus->size(); j++){
      if (m_on_mtaus->at(i).Pt()<m_on_mtaus->at(j).Pt()){
        ontmpmtau = m_on_mtaus->at(i);
        m_on_mtaus->at(i) = m_on_mtaus->at(j);
        m_on_mtaus->at(j) = ontmpmtau; 

        ontmpmtauIDvl = m_on_mtauIDvl->at(i);
        m_on_mtauIDvl->at(i) = m_on_mtauIDvl->at(j);
        m_on_mtauIDvl->at(j) = ontmpmtauIDvl;

        ontmpmtauIDl = m_on_mtauIDl->at(i);
        m_on_mtauIDl->at(i) = m_on_mtauIDl->at(j);
        m_on_mtauIDl->at(j) = ontmpmtauIDl;

        ontmpmtauIDm = m_on_mtauIDm->at(i);
        m_on_mtauIDm->at(i) = m_on_mtauIDm->at(j);
        m_on_mtauIDm->at(j) = ontmpmtauIDm;

        ontmpmtauIDt = m_on_mtauIDt->at(i);
        m_on_mtauIDt->at(i) = m_on_mtauIDt->at(j);
        m_on_mtauIDt->at(j) = ontmpmtauIDt;

        ontmp_mprong = m_on_mprong->at(i);
        m_on_mprong->at(i) = m_on_mprong->at(j);
        m_on_mprong->at(j) = ontmp_mprong;

        ontmp_mrnn = m_on_mrnn->at(i);
        m_on_mrnn->at(i) = m_on_mrnn->at(j);
        m_on_mrnn->at(j) = ontmp_mrnn;
      }
    }
  }
  m_non_mtaus->push_back(counton);
  */
  /*
  // Retrieve the features of an object for a specific trigger (eta trigger)
  auto cg_eta = m_trigDecTool->getChainGroup(trigHLTetar22);
  auto fc_eta = cg_eta->features();
  auto tauFeatureContainers_eta = fc_eta.containerFeature<xAOD::TauJetContainer>("TrigTauRecMerged_MVA");
  m_on_mtaus_eta->clear();
  m_on_mtauIDvl_eta->clear();
  m_on_mtauIDl_eta->clear();
  m_on_mtauIDm_eta->clear();
  m_on_mtauIDt_eta->clear();
  m_on_mprong_eta->clear();
  m_on_mrnn_eta->clear();
  m_non_mtaus_eta->clear();
  int counton_eta = 0;
  for (auto taucont_eta : tauFeatureContainers_eta){
    for (auto tau_eta : *taucont_eta.cptr()){
      if (tau_eta->pt() * 0.001 < 10.) continue;
      TLorentzVector ontau_eta; 
      ontau_eta.SetPtEtaPhiM(tau_eta->pt() * 0.001, tau_eta->eta(), tau_eta->phi(), 0);
      m_on_mtaus_eta->push_back(ontau_eta);
      m_on_mtauIDvl_eta->push_back(tau_eta->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigVeryLoose));
      m_on_mtauIDl_eta->push_back(tau_eta->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigLoose));
      m_on_mtauIDm_eta->push_back(tau_eta->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigMedium));
      m_on_mtauIDt_eta->push_back(tau_eta->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigTight));
      m_on_mprong_eta->push_back(tau_eta->nTracks());
      m_on_mrnn_eta->push_back(tau_eta->discriminant(xAOD::TauJetParameters::TauID::RNNJetScoreSigTrans));
      counton_eta ++;
    }
  }
  // Sorting Loop
  TLorentzVector ontmpmtau_eta;
  bool ontmpmtauIDvl_eta, ontmpmtauIDl_eta, ontmpmtauIDm_eta, ontmpmtauIDt_eta; 
  size_t ontmp_mprong_eta; 
  float ontmp_mrnn_eta;
  for ( int i =0 ; i < (int)m_on_mtaus_eta->size() - 1; i++){
    for (int j = i + 1; j < (int)m_on_mtaus_eta->size(); j++){
      if (m_on_mtaus_eta->at(i).Pt()<m_on_mtaus_eta->at(j).Pt()){
        ontmpmtau_eta = m_on_mtaus_eta->at(i);
        m_on_mtaus_eta->at(i) = m_on_mtaus_eta->at(j);
        m_on_mtaus_eta->at(j) = ontmpmtau_eta; 

        ontmpmtauIDvl_eta = m_on_mtauIDvl_eta->at(i);
        m_on_mtauIDvl_eta->at(i) = m_on_mtauIDvl_eta->at(j);
        m_on_mtauIDvl_eta->at(j) = ontmpmtauIDvl_eta;

        ontmpmtauIDl_eta = m_on_mtauIDl_eta->at(i);
        m_on_mtauIDl_eta->at(i) = m_on_mtauIDl_eta->at(j);
        m_on_mtauIDl_eta->at(j) = ontmpmtauIDl_eta;

        ontmpmtauIDm_eta = m_on_mtauIDm_eta->at(i);
        m_on_mtauIDm_eta->at(i) = m_on_mtauIDm_eta->at(j);
        m_on_mtauIDm_eta->at(j) = ontmpmtauIDm_eta;

        ontmpmtauIDt_eta = m_on_mtauIDt_eta->at(i);
        m_on_mtauIDt_eta->at(i) = m_on_mtauIDt_eta->at(j);
        m_on_mtauIDt_eta->at(j) = ontmpmtauIDt_eta;

        ontmp_mprong_eta = m_on_mprong_eta->at(i);
        m_on_mprong_eta->at(i) = m_on_mprong_eta->at(j);
        m_on_mprong_eta->at(j) = ontmp_mprong_eta;

        ontmp_mrnn_eta = m_on_mrnn_eta->at(i);
        m_on_mrnn_eta->at(i) = m_on_mrnn_eta->at(j);
        m_on_mrnn_eta->at(j) = ontmp_mrnn_eta;
      }
    }
  }
  m_non_mtaus_eta->push_back(counton_eta);
  */

  //Online Taus
  const xAOD::TauJetContainer* ontaus = nullptr; 
  ANA_CHECK (evtStore()->retrieve (ontaus, "HLT_TrigTauRecMerged_MVA")); //r22: HLT_TrigTauRecMerged_MVA, r21: HLT_xAOD__TauJetContainer_TrigTauRecMerged
  //ANA_CHECK (evtStore()->retrieve (ontaus, "HLT_tau35_perf_tracktwo_tau25_perf_tracktwo"));
  //Clear Vectors
  m_non_TrigTRM->clear();
  m_on_TrigTRM->clear();
  m_on_idvlTrigTRM->clear();
  m_on_idlTrigTRM->clear();
  m_on_idmTrigTRM->clear();
  m_on_idtTrigTRM->clear();
  m_on_prngTrigTRM->clear();
  m_on_rnnTrigTRM->clear();

  int countonTRM = 0;

  for (auto ontau : *ontaus){
    if (ontau->pt() * 0.001 < 10.) continue; 

    TLorentzVector ontauTRM; 
    ontauTRM.SetPtEtaPhiM(ontau->pt() * 0.001, ontau->eta(), ontau->phi(), 0);
    m_on_TrigTRM->push_back(ontauTRM);
    m_on_idvlTrigTRM->push_back(ontau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigVeryLoose));
    m_on_idlTrigTRM->push_back(ontau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigLoose));
    m_on_idmTrigTRM->push_back(ontau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigMedium));
    m_on_idtTrigTRM->push_back(ontau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigTight));
    m_on_prngTrigTRM->push_back(ontau->nTracks());
    m_on_rnnTrigTRM->push_back(ontau->discriminant(xAOD::TauJetParameters::TauID::RNNJetScoreSigTrans));
    countonTRM++;
  }
  // Outside Tau for loop do sorting (without truth matching)
  TLorentzVector ontrmtau;
  bool ontrmtauIDvl, ontrmtauIDl, ontrmtauIDm, ontrmtauIDt; 
  size_t ontrm_prong; 
  float ontrm_rnn;
  for ( int i =0 ; i < (int)m_on_TrigTRM->size() - 1; i++){
    for (int j = i + 1; j < (int)m_on_TrigTRM->size(); j++){
      if (m_on_TrigTRM->at(i).Pt()<m_on_TrigTRM->at(j).Pt()){
        ontrmtau = m_on_TrigTRM->at(i);
        m_on_TrigTRM->at(i) = m_on_TrigTRM->at(j);
        m_on_TrigTRM->at(j) = ontrmtau; 

        ontrmtauIDvl = m_on_idvlTrigTRM->at(i);
        m_on_idvlTrigTRM->at(i) = m_on_idvlTrigTRM->at(j);
        m_on_idvlTrigTRM->at(j) = ontrmtauIDvl;

        ontrmtauIDl = m_on_idlTrigTRM->at(i);
        m_on_idlTrigTRM->at(i) = m_on_idlTrigTRM->at(j);
        m_on_idlTrigTRM->at(j) = ontrmtauIDl;

        ontrmtauIDm = m_on_idmTrigTRM->at(i);
        m_on_idmTrigTRM->at(i) = m_on_idmTrigTRM->at(j);
        m_on_idmTrigTRM->at(j) = ontrmtauIDm;

        ontrmtauIDt = m_on_idtTrigTRM->at(i);
        m_on_idtTrigTRM->at(i) = m_on_idtTrigTRM->at(j);
        m_on_idtTrigTRM->at(j) = ontrmtauIDt;

        ontrm_prong = m_on_prngTrigTRM->at(i);
        m_on_prngTrigTRM->at(i) = m_on_prngTrigTRM->at(j);
        m_on_prngTrigTRM->at(j) = ontrm_prong;

        ontrm_rnn = m_on_rnnTrigTRM->at(i);
        m_on_rnnTrigTRM->at(i) = m_on_rnnTrigTRM->at(j);
        m_on_rnnTrigTRM->at(j) = ontrm_rnn;
      }
    }
  }

  m_non_TrigTRM->push_back(countonTRM); 
  
  //Offline Taus
  const xAOD::TauJetContainer* taus = nullptr; 
  ANA_CHECK (evtStore()->retrieve (taus, "TauJets"));
  
  //Clear the vectors 
  //Taus without Matching 
  /*
  m_noff_taus->clear();
  m_off_taus->clear();
  m_off_tauIDvl->clear();
  m_off_tauIDl->clear();
  m_off_tauIDm->clear();
  m_off_tauIDt->clear();
  m_off_prong->clear();
  m_off_rnn->clear();
  */
  //Taus with matching
  m_noff_mtaus->clear();
  m_off_mtaus->clear();
  m_off_mtaus->clear();
  m_off_mtauIDvl->clear();
  m_off_mtauIDl->clear();
  m_off_mtauIDm->clear();
  m_off_mtauIDt->clear();
  m_off_mprong->clear();
  m_off_mrnn->clear();

  int countoff = 0; 
  int countmoff = 0;
  for (auto tau : *taus){ //replaced const xAOD::TauJet* with auto 
      if (tau->pt() * 0.001 < 10.) continue; 
      //ANA_MSG_INFO ("in execute, tau pT = " << tau->pt() * 0.001 ); 
      //push back vectors
      TLorentzVector offtau; 
      offtau.SetPtEtaPhiM(tau->pt() * 0.001, tau->eta(), tau->phi(), 0);
      /*
      m_off_taus->push_back(offtau);
      m_off_tauIDvl->push_back(tau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigVeryLoose));
      m_off_tauIDl->push_back(tau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigLoose));
      m_off_tauIDm->push_back(tau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigMedium));
      m_off_tauIDt->push_back(tau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigTight));
      m_off_prong->push_back(tau->nTracks());
      m_off_rnn->push_back(tau->discriminant(xAOD::TauJetParameters::TauID::RNNJetScoreSigTrans));
      */
      countoff++;

      auto TruthTau = m_T2MT->getTruth(*tau);
      if (TruthTau){
        TLorentzVector offmatchtau;
        offmatchtau.SetPtEtaPhiM(tau->pt() * 0.001, tau->eta(), tau->phi(), 0);
        m_off_mtaus->push_back(offmatchtau);
        m_off_mtauIDvl->push_back(tau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigVeryLoose));
        m_off_mtauIDl->push_back(tau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigLoose));
        m_off_mtauIDm->push_back(tau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigMedium));
        m_off_mtauIDt->push_back(tau->isTau(xAOD::TauJetParameters::IsTauFlag::JetRNNSigTight));
        m_off_mprong->push_back(tau->nTracks());
        m_off_mrnn->push_back(tau->discriminant(xAOD::TauJetParameters::TauID::RNNJetScoreSigTrans));
        countmoff++;
      }
  }
  // Outside Tau for loop do sorting (without truth matching)
  /*
  TLorentzVector tmptau;
  bool tmptauIDvl, tmptauIDl, tmptauIDm, tmptauIDt; 
  size_t tmp_prong; 
  float tmp_rnn;
  for ( int i =0 ; i < (int)m_off_taus->size() - 1; i++){
    for (int j = i + 1; j < (int)m_off_taus->size(); j++){
      if (m_off_taus->at(i).Pt()<m_off_taus->at(j).Pt()){
        tmptau = m_off_taus->at(i);
        m_off_taus->at(i) = m_off_taus->at(j);
        m_off_taus->at(j) = tmptau; 

        tmptauIDvl = m_off_tauIDvl->at(i);
        m_off_tauIDvl->at(i) = m_off_tauIDvl->at(j);
        m_off_tauIDvl->at(j) = tmptauIDvl;

        tmptauIDl = m_off_tauIDl->at(i);
        m_off_tauIDl->at(i) = m_off_tauIDl->at(j);
        m_off_tauIDl->at(j) = tmptauIDl;

        tmptauIDm = m_off_tauIDm->at(i);
        m_off_tauIDm->at(i) = m_off_tauIDm->at(j);
        m_off_tauIDm->at(j) = tmptauIDm;

        tmptauIDt = m_off_tauIDt->at(i);
        m_off_tauIDt->at(i) = m_off_tauIDt->at(j);
        m_off_tauIDt->at(j) = tmptauIDt;

        tmp_prong = m_off_prong->at(i);
        m_off_prong->at(i) = m_off_prong->at(j);
        m_off_prong->at(j) = tmp_prong;

        tmp_rnn = m_off_rnn->at(i);
        m_off_rnn->at(i) = m_off_rnn->at(j);
        m_off_rnn->at(j) = tmp_rnn;
      }
    }
  }
  */
  // Outside Tau for loop do sorting (WITH matching)
  TLorentzVector tmpmtau;
  bool tmpmtauIDvl, tmpmtauIDl, tmpmtauIDm, tmpmtauIDt; 
  size_t tmp_mprong; 
  float tmp_mrnn;
  for ( int i =0 ; i < (int)m_off_mtaus->size() - 1; i++){
    for (int j = i + 1; j < (int)m_off_mtaus->size(); j++){
      if (m_off_mtaus->at(i).Pt()<m_off_mtaus->at(j).Pt()){
        tmpmtau = m_off_mtaus->at(i);
        m_off_mtaus->at(i) = m_off_mtaus->at(j);
        m_off_mtaus->at(j) = tmpmtau; 

        tmpmtauIDvl = m_off_mtauIDvl->at(i);
        m_off_mtauIDvl->at(i) = m_off_mtauIDvl->at(j);
        m_off_mtauIDvl->at(j) = tmpmtauIDvl;

        tmpmtauIDl = m_off_mtauIDl->at(i);
        m_off_mtauIDl->at(i) = m_off_mtauIDl->at(j);
        m_off_mtauIDl->at(j) = tmpmtauIDl;

        tmpmtauIDm = m_off_mtauIDm->at(i);
        m_off_mtauIDm->at(i) = m_off_mtauIDm->at(j);
        m_off_mtauIDm->at(j) = tmpmtauIDm;

        tmpmtauIDt = m_off_mtauIDt->at(i);
        m_off_mtauIDt->at(i) = m_off_mtauIDt->at(j);
        m_off_mtauIDt->at(j) = tmpmtauIDt;

        tmp_mprong = m_off_mprong->at(i);
        m_off_mprong->at(i) = m_off_mprong->at(j);
        m_off_mprong->at(j) = tmp_mprong;

        tmp_mrnn = m_off_mrnn->at(i);
        m_off_mrnn->at(i) = m_off_mrnn->at(j);
        m_off_mrnn->at(j) = tmp_mrnn;
      }
    }
  }

  //m_noff_taus->push_back(countoff); 
  m_noff_mtaus->push_back(countmoff);  
  
  // Retrieve the eventInfo object from the event store
  /*
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  // print out run and event number from retrieved object
  //ANA_MSG_INFO ("in execute, eventNumber = " << eventInfo->eventNumber() << ", actual bunch crossing = " << eventInfo->actualInteractionsPerCrossing() << "ave bunch crossing = " << eventInfo->averageInteractionsPerCrossing());

  // Read/Fill EventInfo objects
  m_actualBC->clear();
  m_aveBC->clear();
  m_eventNumber = eventInfo->eventNumber();
  m_actualBC->push_back(eventInfo->actualInteractionsPerCrossing());
  m_aveBC->push_back(eventInfo->averageInteractionsPerCrossing());
  */
  // Fill the event into the tree:
  tree ("analysis")->Fill ();

  return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  //ANA_MSG_INFO ("in finalize");
  return StatusCode::SUCCESS;
}